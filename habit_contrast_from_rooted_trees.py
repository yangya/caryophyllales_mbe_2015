"""
input is a directory containing rooted cary clades
the taxon_name_subst_table contains H or W as the last letter of the long name
the last character at each line of taxon table indicates woody or herbacesou
"""

taxa1 = ["CGGO","WOBD"] #Plumbago_auriculata, Limonium_spectabile, both Plumbaginaceae

taxa2 = ["CPKP","JLOV","BLWH","CPLT","EZGR","GCYL","IWIS","KDCH","LLQV","UQCB"]
#Lophophora_williamsii,Pereskia_aculeata;Cactaceae
#Portulaca_mauii,Portulaca_grandiflora,Portulaca_oleracea,Portulaca_suffruticosa,Portulaca_pilosa
#Portulaca_umbraticola,Portulaca_cryptopetala,Portulaca_molokaiensis;Portulacaceae

taxa3 = ["BKQU","MRKX"]
#Phytolacca_americana,Phytolacca_bogotensis;Phytolaccaceae

taxa4 = ["ILU5","AZBL","ILU2","SFKQ"]
#Phytolaccaceae

taxa5 = ["ILU6","JAFJ","JGAB","HMFE","EGOS","ZBTA","VJPU","ILU1"]
#Nyctaginaceae

HABITS = ["H","W"]

import newick3,phylo3,os,sys
import collections

contrast1,contrast2,contrast3,contrast4,contrast5 = [],[],[],[],[]
#store the contrast values

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace[:4]

def get_front_labels(node): #no repeat
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

#Check habits in a clade
#return "W" if all are woody, and return "H" if all are herbaceous
#return NULL if more than 1 type or no type if found
def get_habit(node,habitDICT):
	names = get_front_names(node)
	habits = []
	for name in names:
		hab = habitDICT[name]
		if hab not in habits:
			habits.append(hab)
	if len(habits) == 1:
		return habits[0]
	else: return None

#generate a newick string for the bipartition
def set_to_string(name_set):
	out_string = ""
	for l in name_set:
		out_string += l
		out_string += ","
	return out_string[:-1]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python habit_contrast_from_rooted_trees.py cary_cladesDIR taxon_name_subst_table"
		sys.exit(0)
		
	DIR = sys.argv[1]+"/"
	taxon_table = sys.argv[2]

	#Get the habit for the ingroups
	#each line of taxon table look like: GJNX	Aizoaceae_Cypselea_humifusum_H
	habitDICT = {} #key is name, value is "H" or "W"
	infile = open(taxon_table,"r")
	for line in infile:
		name = line.split("\t")[0]
		habit = (line.strip()).split("_")[-1]
		if habit in HABITS:
			habitDICT[name] = habit
	print "Habit information for",len(habitDICT),"taxa read"

	outfile1eq = open("contrast1_equal_size","w")
	outfile2eq = open("contrast2_equal_size","w")
	outfile3eq = open("contrast3_equal_size","w")
	outfile4eq = open("contrast4_equal_size","w")
	outfile5eq = open("contrast5_equal_size","w")
	outfile1 = open("contrast1","w")
	outfile2 = open("contrast2","w")
	outfile3 = open("contrast3","w")
	outfile4 = open("contrast4","w")
	outfile5 = open("contrast5","w")

	print "Reading extracted clades"
	for i in os.listdir(DIR):
		if ".cary" not in i: continue
		print i
		with open(DIR+i,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		root = intree
		names = get_front_names(root)
		if root.nchildren != 2:
			print "Check rooting. Root should be bifurcating"
			sys.exit()
		for node in root.iternodes(order=1):#POSTORDER,tip to root
			if node.istip: #tips
				node.data['len'] = node.length
			else: #internal nodes or root
				child0,child1 = node.children[0],node.children[1]
				names0,names1 = get_front_names(child0),get_front_names(child1)
				above0,above1 = child0.data['len'],child1.data['len']
				node.data['len'] = ((above0+above1)/2.)+node.length
				habit0 = get_habit(child0,habitDICT)
				habit1 = get_habit(child1,habitDICT)
				if habit0!=None and habit1!=None and habit0!=habit1:
					#found habit contrast
					contrast = abs(above0-above1)/min(above0,above1)
					if above0 > above1 and habit0 == "W":
						contrast = -contrast
					if above0 < above1 and habit1 == "W":
						contrast = -contrast
					if set(get_front_names(node)) <= set(taxa1):
						outfile1.write(str(contrast)+"\n")
						if len(names0) == len(names1):
							outfile1eq.write(str(contrast)+"\n")
					elif set(get_front_names(node)) <= set(taxa2):
						outfile2.write(str(contrast)+"\n")
						if len(names0) == len(names1):
							outfile2eq.write(str(contrast)+"\n")
					elif set(get_front_names(node)) <= set(taxa3):
						outfile3.write(str(contrast)+"\n")
						if len(names0) == len(names1):
							outfile3eq.write(str(contrast)+"\n")
					elif set(get_front_names(node)) <= set(taxa4):
						outfile4.write(str(contrast)+"\n")
						if len(names0) == len(names1):
							outfile4eq.write(str(contrast)+"\n")
					elif set(get_front_names(node)) <= set(taxa5):
						outfile5.write(str(contrast)+"\n")
						if len(names0) == len(names1):
							outfile5eq.write(str(contrast)+"\n")
	outfile1.close()
	outfile2.close()
	outfile3.close()
	outfile4.close()
	outfile5.close()
	outfile1eq.close()
	outfile2eq.close()
	outfile3eq.close()
	outfile4eq.close()
