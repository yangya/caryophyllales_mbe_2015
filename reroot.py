import phylo3,newick3,os,sys

OUTGROUPS = ["Acoe","Alyr","Atha","Brap","Ccle","Cpap","Crub","Csat","Csin","Egra","Fves","Gmax","Grai","Lusi","Mdom","Mesc","Mgut","Mtru","Pper","Ptri","Pvul","Rcom","Slyc","Stub","Tcac","Thal","Vvin"]

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_back_labels(node,root):
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels) #labels do not repeat
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def get_back_names(node,root): #may include duplicates
	back_labels = get_back_labels(node,root)
	return [get_name(i) for i in back_labels]
	
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python reroot.py unrootedtree"
		sys.exit(0)
	
	with open(sys.argv[1],"r") as infile:
		intree = newick3.parse(infile.readline())
			
	#Check outgroup monophyly and reroot
	lvs = intree.leaves()
	outgroup_matches = {} #key is name, value is the tip node object
	curroot = intree
	outgroup_labels = []
	for leaf in lvs: #get all the outgroup labels and tips on the tree
		name = get_name(leaf.label)
		if name in OUTGROUPS:
			outgroup_matches[name] = leaf
			outgroup_labels.append(leaf.label)
	if outgroup_labels == []:
		print "No outgroup in the tree"
	elif len(outgroup_labels) == 1: #one single outgroup
		#cannot reroot on a tip so have to go one more node into the ingroup
		out_mrca = outgroup_matches[get_name(outgroup_labels[0])].parent
		tree = phylo3.reroot(curroot,out_mrca)
		with open(sys.argv[1]+".reroot","w") as outfile:
			outfile.write(newick3.tostring(tree)+";\n")
	else: #has multiple outgroup labels. Check monophyly and reroot
		newroot = None
		for node in curroot.iternodes():
			if node == curroot: continue #skip the root
			front_names = get_front_names(node)
			back_names = get_back_names(node,curroot)
			front_in_names,front_out_names,back_in_names,back_out_names = 0,0,0,0
			for i in front_names:
				if i in OUTGROUPS: front_out_names += 1
				else: front_in_names += 1
			for j in back_names:
				if j in OUTGROUPS: back_out_names += 1
				else: back_in_names += 1
			if front_in_names==0 and front_out_names>0 and back_in_names>0 and back_out_names==0:
				#ingroup at back, outgroup in front
				newroot = node
				break
			if front_in_names>0 and front_out_names==0 and back_in_names==0 and back_out_names>0:
				#ingroup in front, outgroup at back
				newroot = node.parent
				break
		if newroot != None:
			tree = phylo3.reroot(curroot,newroot)
			with open(sys.argv[1]+".reroot","w") as outfile:
				outfile.write(newick3.tostring(tree)+";\n")

			
			
