"""
Find the corresponding go term for each mm tree
Add together all the go annotations for outgorups
"""

import newick3,phylo3,os,sys
import collections

OUTGROUPS = ["Acoe","Alyr","Atha","Brap","Ccle","Cpap","Crub","Csat","Csin","Egra",
			 "Fves","Gmax","Grai","Lusi","Mdom","Mesc","Mgut","Mtru","Pper","Ptri",
			 "Pvul","Rcom","Slyc","Stub","Tcac","Thal","Vvin"] #27 eudicot ougroups

#the pattern for the tip names is taxon_name@seq_name
#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.replace("_R_","")[:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python gene_ontology_mm.py list mmDIR pep/dna outfile"
		sys.exit(0)
	
	#get the relationship between seq and GO
	infile = open(sys.argv[1],"r")
	goDICT = {} #key is sequence id, value is a set of GO terms
	for line in infile:
		if "GO:" not in line: continue #only look at genes with GO
		spls = (line.strip()).split("\t")
		goDICT[spls[0].replace("-","_")] = set(spls[1].split(","))
	infile.close()
	
	out_ids = [] #store all outgroup ids to be matched to GO terms
	mmDIR = sys.argv[2]+"/"
	outfile = open(sys.argv[4],"w")
	for treefile in os.listdir(mmDIR):
		if treefile[-3:] != ".mm": continue
		print treefile
		with open(mmDIR+treefile,"r") as infile:
			intree = newick3.parse(infile.readline())
		
		homologGO = ""
		labels = get_front_labels(intree)
		for label in labels:
			if get_name(label) in OUTGROUPS:
				if sys.argv[3] == "dna":
					seqidmatch = label.split("__")[1]
				elif sys.argv[3] == "pep":
					seqidmatch = (label.split("__")[2]).replace("_peptide","")
				seqidmatch = seqidmatch.replace("-","_")
				if seqidmatch in goDICT:
					seqGO = goDICT[seqidmatch]
					if homologGO == "":
						homologGO = seqGO
					elif homologGO != seqGO:
						homologGO = homologGO.union(seqGO)
		if homologGO != "":
			GOout = ','.join(homologGO)
			outfile.write(treefile.replace(".mm","")+"\t"+GOout+"\n")
	outfile.close()
