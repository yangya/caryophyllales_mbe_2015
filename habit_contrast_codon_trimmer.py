"""
Input a dir of codon alignments that ends with ".pal2nal" in fasta format
Trim alignment
replace stop codon
output alignment in paml format
"""

import os,sys

class Sequence:
	def __init__(self, inlab, inseq):
		self.lab = inlab
		self.name = inlab
		self.seq = inseq
		self.qualstr = ""
		self.qualarr = []
	
	def set_qualstr(self, qual):
		self.qualstr = qual
		for j in qual:
			self.qualarr.append(ord(j)-33)
	
	def get_fancy(self):
		y = "Sequence label: "+self.lab+"\nSequence: "+self.seq+"\n"
		return y

	def get_fasta(self):
		return ">"+self.lab+"\n"+self.seq+"\n"

	def rev_comp(self):
		tseq = ""
		for i in self.seq[::-1]:
			if i.lower() == "a":
				tseq += "t"
			elif i.lower() == "t":
				tseq += "a"
			elif i.lower() == "c":
				tseq += "g"
			elif i.lower() == "g":
				tseq += "c"
			else:
				tseq += i
		self.seq = tseq
		
def read_fasta_file(filename):
	fl = open(filename,"r")
	maind = [] #maind[key] = value
	templab = ""
	tempseq = ""
	first = True
	for i in fl:
		if i[0] == ">":
			if first == True:
				first = False
			else:#if not first store lastseq read
				ts = Sequence(templab,tempseq)
				maind.append(ts)
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	fl.close()
	ts = Sequence(templab,tempseq)
	maind.append(ts)
	return maind

missingset = set("-?N")
stop_codons = ["TAG","TAA","TGA"]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python "+sys.argv[0]+" fraction_to_keep(0-1) DIR"
		sys.exit(0)
	frac = float(sys.argv[1])
	DIR = sys.argv[2]+"/"
	for aln in os.listdir(DIR):
		if aln[-8:] != ".pal2nal": continue
		seqs = read_fasta_file(DIR+aln)
		keepcodons = []
		length = len(seqs[0].seq)
		num_seqs = len(seqs)
		if length % 3 != 0:
			print "these seem to not be codons because they don't divide by 3"
			print "length:",length
			sys.exit(0)
		length3 = length/3
		for i in range(length3): #i is the codon pointer
			count = 0
			for j in seqs:
				codon = j.seq[(i*3):(i*3)+3].upper()
				if len(missingset - set(codon)) == len(missingset):
					count += 1
			#print count,float(count)/num_seqs,frac
			if float(count)/num_seqs >= frac:
				keepcodons.append(i)
		if len(keepcodons) == 0:
			print "this has no remaining codons"
		
		#this writes fasta output
		"""
		outfile = open(sys.argv[3],"w")
		for i in seqs:
			outfile.write(">"+i.name+"\n")
			for j in range(length3):
				if j in keepcodons:
					codon = ""
					codon += i.seq[(j*3):(j*3)+3]
					if codon not in stop_codons:
						outfile.write(codon)
					else: outfile.write("???")
			outfile.write("\n")
		outfile.close()"""
		
		#write paml output
		outfile = open(DIR+aln+".trim.paml","w")
		outfile.write(str(len(seqs))+" "+str(len(keepcodons)*3)+"\n")
		for i in seqs:
			outfile.write(i.name+"\n")
			for j in range(length3):
				if j in keepcodons:
					codon = ""
					codon += i.seq[(j*3):(j*3)+3]
					if codon not in stop_codons:
						outfile.write(codon)
					else: outfile.write("???")
			outfile.write("\n")
		outfile.close()
