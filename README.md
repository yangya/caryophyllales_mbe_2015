Citation: Yang, Y. et al. Dissecting molecular evolution in the highly diverse plant clade Caryophyllales using transcriptome sequencing. In revision


All Caryophyllales sequence IDs are formated as taxonID@seqID, with the taxonID being a 4-letter code. All non-Caryophyllales sequence IDs do not have "@". We use the first 4 letters to identify each taxon in both ingroups and outgroups, and use "@" to identify the Caryophyllales taxa.

All scripts are hard-coded for the Caryophyllales data set. Modify it as needed for you own data sets.

##Clustering and cleaning homolog groups
###cd-hit to reduce highly similar sequences
	
	cd-hit -i taxonID.pep.fa -o taxonID.cd-hit -c 0.99 -n 5 -T 2

###Inferring putative homolog groups using similarity

All-by-all blastp
		
	cat *.cd-hit >all.pep.fa	
	makeblastdb -in all.pep.fa -parse_seqids -dbtype prot -out all.pep.fa
	python blastp_wrapper.py

Filter blastp output by similarity and coverage

	sh blast_to_mcl.sh

Clustering using mcl with an inflation value of 1.4 and an evalue cutoff of e-30

	mcl all.evalue-log --abc -te 5 -tf 'gq(30)' -I 1.4 -o I1.4_e30

Write individual fasta files for clusters from the mcl output that have at least 8 ingroup taxa
	
	mkdir clusters_I1.4_96taxa
	write_fasta_files_from_mcl.py all.pep.fa I1.4_e30 8 clusters_I1.4_96taxa

###Iterative aligning and cutting long branches from putative homolog groups

Build initial alignments using mafft

	python mafft_wrapper.py <inDIR> <outDIR> .fa <thread> aa

Trim alignments using phyutility

	python phyutility_wrapper.py <inDIR> <outDIR> 0.05 aa

Initial tree search using FastTree

	python fasttree_wrapper.py <inDIR> <outDIR> aa

Refining homologs iteratively. Input directory should contain files clusterID.fa, clusterID.fa.aln (alignment from clusterID.fa), clusterID.fa.aln-cln (trimmed alignment) and clusterID.fa.aln-cln.fasttree (tree).

	python cut_long_branches_iter.py inDIR outDIR >log

The align and refine the final clusters.
	
	python mafft_wrapper.py <inDIR> <outDIR> .fa <thread> aa
	python sate_wrapper.py <inDIR> <outDIR> <no. cores> aa
	python phyutility_wrapper.py <inDIR> <outDIR> 0.1 aa

Move all the refined final alignment into a new directory. cd into the new directory and tree inference using raxml

	python raxml_wrapper.py <number_cores> aa

Cut long internal branches

	python cut_long_internal_branches.py inDIR internal_branch_length_cutoff minimal_taxa outDIR

Trim spurious tips
	
	python trim_tips.py <treDIR> <outDIR> <relative_cutoff> <absolute_cutoff>

Mask monophyletic and paraphyletic tips that belongs to the same taxon. Change ALIGNMENT_FILE_ENDING to match the corresponding alignment files

	python mask_tips_by_taxonID_transcripts.py <treDIR> <aln-clnDIR> <outDIR>

[Note Oct 6, 2014] Both MAFFT and Sate had updates since I finished cleaning up homologs. Instead of doing iterative cutting and re-aligning with MAFFT and fasttree, it is much more efficient to align with PASTA or MAFFT, cut long branches, and align at most two more times before estimating the final homolog trees. I have since switched to the more computationally efficient homolog building procedure in https://bitbucket.org/yangya/phylogenomic_dataset_construction and stopped updating the iterative aligning scripts provided here. In another word, it's easier to let PASTA to take care of the iterative alignment task.


##Orthology and species tree inference
###Extract rooted ingroup clades and seprate paralogs

	python python prune_paralogs_RT.py <homoTreeDIR> <outDIR>

Summarize matrix occupancy stats

	python ortholog_occupancy_stats.py <ortho_treDIR>

Plot the ranked number of taxa per ortholog in R:

	a = read.table('ortho_stats')
	downo = order(a[,1],decreasing=T)
	pdf(file="taxon_occupancy.pdf")
	plot(a[downo,1])
	dev.off()

Write aligned fasta files for each ortholog group

	python write_alignments_from_orthologs.py <final alnignment DIR> <orthoTreeDIR> <outDIR>

Trim ortholog alignments

	python phyutility_wrapper.py <inDIR> <outDIR> 0.3 aa

###Estimate species tree by concatenation

Choose the minimal cleaned alignment length and minimal number of taxa filters. Concatenate selected cleaned matrices:

	python concatenate_matrices.py <aln-clnDIR> 150 90 aa filter150-90
	python concatenate_matrices.py <aln-clnDIR> 150 85 aa filter150-85

Run raxml with 200 rapid bootstrap replicates and search for the best tree.

	raxmlHPC-PTHREADS-SSE3 -T 9 -f a -x 12345 -# 200 -p 12345 -m PROTCATWAG -q <.models file> -s <.phy file> -n <output>

Run 200 jackknife replicates. Copy both the .phy and .model file into a new working dir, cd into the working dir, and run the following script (sample fixed proportion of number of genes):

	python jackknife_by_percent_genes.py <number_cores> <subsampling ratio, 0.1 or 0.3> aa

Mapping jackknife results to the best tree.

	cat *result* >../JK10_trees
	raxmlHPC-PTHREADS-SSE3 -f b -t <bestTree> -z JK10_trees -T 2 -m PROTCATWAG -n <output_name>

Translate taxon codes to make the tree more readable. Each line of the taxon_table have the short name and the full name separated by a tab.
	
	python taxon_name_subst.py <taxon_name_subst_table> <treefile>

###Estimating species tree using ASTRAL

The input trees can be any orthologous trees. Here we use 1 to 1 orthologs. 
Copy the selected trimmed alignments to a new DIR, cd into the new DIR and estimate gene trees with 200 bootstrap replicates

	python raxml_bs_wrapper.py <number_cores> aa

Re-format homolog trees and prepare input files for ASTRAL

	python filter_1to1_orthologs_for_ASTRAL.py <homoTreeDIR> <minimal_taxa> <out dir>
	
There are two input tree files for ASTRAL. The first one contains all the best trees from orthologs

	cat *.tre >all_ML.trees
	
If doing bootstrap in ASTRAL, a second file is needed with paths to all the bootstrap trees

	ls *.trees >all_boot.trees

Run ASTRAL and two-stage bootstrap

	java -jar astral.4.7.3.jar -i all_ML.trees -b all_boot.trees -g -r 100 -o astral_boot.out

The last tree in astral_boot.out is the tree we want

	tail -1 astral_boot.out >astral_boot.tre

##Inferences based on homolog trees
###Extract rooted clades from homologs.

	python extract_clades.py <inDIR> <treefileending> <outDIR> <minimal_ingroup_taxa> <taxon_code> <output_name>

For example, to extract rooted Caryophyllales clades of at least eight taxa, the command is
	
	python extract_clades.py <inDIR> .mm <inDIR> 8 taxon_code cary

###Woody/herbaceous contrast
Calculate contrasts on raxml trees by step-wise averaging branch lengths on both the woody and the herbaceous sides. I removed results for this analysis from the manuscript per reviewer's suggestion due to redundancy. However, it is good to plot the contrast from the peptide trees before back translation for a quick check.

	python habit_contrast_pep.py <cary_cladesDIR> <taxon_table>

Prepare trees and codon alignments for condon contrast. Input is the concatenated fasta file of all ingroup peptide sequences, another fasta file of all ingroup CDS, and the dir with extracted rooted cary clades

	python habit_contrast_codon_prepare_alignments.py <pep_fasta> <cds_fasta> <cary_clade_dir> <out_dir>

Trim codon alignment by minimal unambiguous codon occupancy of 0.2, mask stop codon by "???", and convert fasta to paml format

	python habit_contrast_codon_trimmer.py 0.2 <pal2nal_output_dir>

Label a woody clade with "#1" and its sister herbacesou clade with "#2". Write a control file for each sister pair on every gene tree and run codeml.

	python habit_contrast_codon_A.py <treDIR> <codon_alnDIR> <taxon_name_subst_table>
	python habit_contrast_codon_B.py <treDIR> <codon_alnDIR> <taxon_name_subst_table>
	python habit_contrast_codon_C.py <treDIR> <codon_alnDIR> <taxon_name_subst_table>
	python habit_contrast_codon_D.py <treDIR> <codon_alnDIR> <taxon_name_subst_table>
	python habit_contrast_codon_E.py <treDIR> <codon_alnDIR> <taxon_name_subst_table>

Parse codeml output. Chose the parser for the contrast. The parser for contrasts A, B and D reads an additional log file.

	python habit_contrast_codon_output_parser_CE.py <codemlDIR> <outfile>
	python habit_contrast_codon_output_parser_ABD.py <codemlDIR> <outfile>

Sign test in R. Count the number of contrasts that are higher than zero and lower than zero

	binom.test(# positive values, # positive and negative vaslues)

###Summerize clade size and taxon repeats

	python count_taxon_repeats.py <tree DIR> <outFILE>

###Reroot the species tree

	python reroot.py <unrootedtree file>

###Map gene duplications
Count gene duplications from rooted clades extracted from homolog trees, and map their frequencies to a rooted species tree. Change the average bootstrap filter (BOOT_FILTER) accordingly.

	python map_dups_bootfilter.py <cary_cladeDIR> <ast_cladeDIR> <ros_cladeDIR> <rooted_species_tree> <output_file_name>

###Get a representative *Arabidopsis thaliana* locus ID for each extracted clade

	python get_arabidopsis_locus_id_for_inclade.py <incladeDIR> <homologDIR> <outfile>

###Ks plots
Prepare the cds fasta file named taxonID.cds.fa and the corresponding peptide fasta file named taxonID.pep.fa. Make sure that the sequence IDs in the two files are the same for each gene. The ks calculater scripts are from https://github.com/tanghaibao/bio-pipeline/tree/master/synonymous_calculation. cd into the directory where taxonID.pep.fa and taxonID.cds.fa are located. Make sure that cd-hit is in the path and is executable. Put synonymous_calc.py and bin from the ks-calculator in the current directory

	python ks_plots.py taxonID num_cores taxon_name_table

It output an R scripts. Run it in R and it will produce the pdf files of the Ks plots

