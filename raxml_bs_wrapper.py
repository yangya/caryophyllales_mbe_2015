"""
Input: a dir of cleaned alignments in fasta format and end with "-cln"
Output: trees estimated by raxml
"""

ALIGNMENT_FILE_ENDING = ".aln-cln"
#ALIGNMENT_FILE_ENDING = ".phy"
import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python raxml_bs_wrapper.py number_cores DNA/aa"
		sys.exit(0)
	
	num_cores = sys.argv[1]
	if sys.argv[2] == "aa":
		m = "PROTCATWAG"
	elif sys.argv[2] == "DNA":
		m = "GTRCAT" 	
	else:
		print "Input data type: DNA or aa"
		sys.exit()
	DIR = "./"
	
	done = [] #record files finished
	for i in os.listdir(DIR):
		if "RAxML_bestTree" in i:
			print i.split(".")[1]
			done.append(i.split(".")[1]) #recored the clusterIDs of the ones that are done
	print done
	
	for j in os.listdir(DIR):
		if j[-len(ALIGNMENT_FILE_ENDING):] != ALIGNMENT_FILE_ENDING:
			continue
		if j.split(".")[0] in done:
			continue
		cmd = "raxmlHPC-PTHREADS-SSE3 -T "+num_cores+" -f a  -x 12345 -# 200 -p 12345 -s "+j+" -n "+j+" -m "+m
		print cmd
		os.system(cmd)

