import sys,os,newick3,phylo3

"""
start from fasttree results from the first round of alignment
not automating the first round so that one can check the data

Make alignment and then tree
walk through the tree and cut internal branches longer than the first cutoff
re-align until no internal branch is longer than the first cutoff

then cut branches longer than the second cutoff
re-align until no branch is longer than the second cutoff

write new fasta files for sate
"""

MIN_TAXA = 8
NUM_MAFFT_CORES = 6
#number of processors mafft uses
#generally should be less than 10

#if taxon id pattern changes, change it here
def get_name(name):
	return name.split("@")[0]
	
def get_leaf_labels(leaves):
	labels = []
	for i in leaves:
		labels.append(i.label)
	return labels

def count_taxa(node):
	labels = get_leaf_labels(node.leaves())
	names = []
	for label in labels:
		names.append(get_name(label))
	return len(set(names))

def find_longest_branch_length(tree):
	longest_branch_length = 0.0
	for node in tree.iternodes():
		if node != tree:
			longest_branch_length = max(longest_branch_length,node.length)
	return longest_branch_length

#do not score terminal branches
#long terminal branches usually do not disturb the alignment as much
def find_longest_internal_branch_length(tree):
	longest_internal_branch_length = 0.0
	for node in tree.iternodes():
		if node != tree and not node.istip:
			longest_internal_branch_length = max(longest_internal_branch_length,node.length)
	return longest_internal_branch_length

#smooth the kink created by prunning
#to prevent creating orphaned tips after prunning twice at the same node
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip internal node
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: #tree has >=4 leaves so the other node cannot be tip
			curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	if node.nchildren == 1:
		length = node.length + (node.children[0]).length
		par = node.parent
		kink = node
		node = node.children[0]
		#parent--kink---node<
		par.remove_child(kink)
		par.add_child(node)
		node.length = length
	return node,curroot

#cut long branches and output all subtrees regardless of size
def cut_long_branches(curroot,cutoff):
	going = True
	subtrees = [] #store all subtrees after cutting
	if curroot.nchildren == 2: #fix the root
		#move the root away to an adjacent none-tip internal node
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: #tree has >=4 leaves so the other node cannot be tip
			curroot = phylo3.reroot(curroot,curroot.children[0])
	while going:
		going = False #only keep going if long branches were found during last round
		for node in curroot.iternodes(): #Walk through nodes
			if node != curroot and node.length > cutoff:
				subtrees.append(node)
				node = node.prune()
				if len(curroot.leaves()) >= 4:
					node,curroot = remove_kink(node,curroot)
					going = True
				break
	subtrees.append(curroot) #write out the residue after cutting
	return subtrees

def mafft_align(fasta_file):
	count = 0 #record how many sequences in the fasta file
	with open(fasta_file,"r") as infile:
		for line in infile:
			if line[0] == ">": count += 1
	if count >= 1000: alg = "--auto" #so that the run actually finishes!
	else: alg = "--genafpair --maxiterate 1000"
	com = "mafft "+alg+" --anysymbol --amino --thread "
	com += str(NUM_MAFFT_CORES)+" "+fasta_file+" > "+fasta_file+".aln"
	print com
	os.system(com)
	return fasta_file+".aln"

#give the path of the alignment file
#remove columns with occupancy lower than 0.1
#remove seqs shorter than 40 bp after filter columns
def phyutility(alignment,cleaned,occupancy):
	cmd = "phyutility -aa -clean "+str(occupancy)
	cmd += " -in "+alignment+" -out "+alignment+"-pht"
	print cmd
	os.system(cmd)
	seqid = ""
	first = True
	infile = open(alignment+"-pht","r")
	outfile = open(cleaned,"w")
	for line in infile:
		line = line.strip()
		if len(line) == 0: continue #skip empty lines
		if line[0] == ">":
			if seqid != "": #not at the first seqid
				if first == True:
					length = len(seq)
					min_chr = max(float(occupancy)*length,40)
					first = False
					print "length",length,"min_chr",min_chr
				if len(seq.replace("-","")) >= min_chr:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			seqid,seq = line.split(" ")[0][1:],""
		else: seq += line.strip()
	#process the last seq
	if len(seq.replace("-","")) >= min_chr:
		outfile.write(">"+seqid+"\n"+seq+"\n")
	infile.close()
	outfile.close()


#give the path of the alignment file
#remove columns with occupancy lower than 0.1
#remove seqs shorter than 50 bp after filter columns
def trimal(alignment,cleaned,occupancy):
	cmd = "trimal -in "+alignment+" -out "+alignment+"-trim"+" -gt "
	cmd += str(occupancy)+" -st 0.0005"
	print cmd
	os.system(cmd)
	seqid = ""
	first = True
	infile = open(alignment+"-trim","r")
	outfile = open(cleaned,"w")
	for line in infile:
		line = line.strip()
		if len(line) == 0: continue #skip empty lines
		if line[0] == ">":
			if seqid != "": #not at the first seqid
				if first == True:
					length = len(seq)
					min_chr = max(float(occupancy)*length,40)
					first = False
					print "length",length,"min_chr",min_chr
				if len(seq.replace("-","")) >= min_chr:
					outfile.write(">"+seqid+"\n"+seq+"\n")
			seqid,seq = line.split(" ")[0][1:],""
		else: seq += line.strip()
	#process the last seq
	if len(seq.replace("-","")) >= min_chr:
		outfile.write(">"+seqid+"\n"+seq+"\n")
	infile.close()
	outfile.close()


def fasttree(cleaned_alignment):
	com = "FastTreeMP -wag "+cleaned_alignment+" >"+cleaned_alignment+".tre"
	print com
	os.system(com)
	return cleaned_alignment+".tre"

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python pep_cut_long_branches_iter.py inDIR outDIR cutoff1 cutoff2 out_error"
		sys.exit(0)
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	BRANCH_LENGTH_CUTOFF1 = float(sys.argv[3])
	BRANCH_LENGTH_CUTOFF2 = float(sys.argv[4])
	out_error = sys.argv[5] #record empty files after iterative cutting and trimming
	
	for i in os.listdir(inDIR): #go through fasta files in the input directory
		if i[-9:] != ".fasttree": continue
		tree_file = inDIR+i
		#fasta_file = tree_file.replace(".aln-cln.fasttree","")
		fasta_file = inDIR+i.split(".")[0]+".fa.cut"
		with open(tree_file,"r") as infile:
			first_line = infile.readline() #there's only 1 tree in each file
		if first_line.strip() == "": continue #empty file after trimming
		intree = newick3.parse(first_line.replace ("-","_"))
		if count_taxa(intree) < MIN_TAXA:
			continue #skip trees with few ingroups
		clusterID = i.split(".")[0] #looks like cluster9
		trees = [intree]

		#read fasta file. not using biopython so that can run on clusters
		infile = open(fasta_file,"r")
		seqDICT = {} #key is seq id, value is the sequence
		seqid = ""
		for line in infile:
			line = line.strip()
			if len(line) == 0: continue #skip empty lines
			if line[0] == ">":
				if seqid != "":
					seqDICT[seqid] = seq
					if "-" in seqid:
						print "Fournd special characters '-' in",seqid
						sys.exit()
				seqid,seq = line[1:],""
			else: seq += line.strip()
		seqDICT[seqid] = seq #add the last record
		infile.close()

		"""Step1: cut with the first branch length cutoff"""
		longest_br_len = find_longest_branch_length(intree)
		if longest_br_len < BRANCH_LENGTH_CUTOFF2:
			#the input tree has no branch longer than the second cut
			#copy out the original fasta file and alignment
			os.system("cp "+fasta_file+" "+outDIR+clusterID+"-1.final.fa")
			os.system("cp "+fasta_file+".aln "+outDIR+clusterID+"-1.final.fa.aln")
			continue
		elif longest_br_len < BRANCH_LENGTH_CUTOFF1:
			#if the input tree the longest branch between first and second cutoffs
			print "Re-estimateing branch lengths"
			trimal(fasta_file+".aln",fasta_file+".aln-trimal-cln",0.3)
			newtreefile = fasttree(fasta_file+".aln-trimal-cln")
			with open(newtreefile) as infile: #update the tree
				trees = [newick3.parse(infile.readline())]
				#this new tree will go into the second cutting step
		else:
			if longest_br_len > 3.0: #very, very messy cluster
				cutoff1 = max(BRANCH_LENGTH_CUTOFF1,2.0)
			else: cutoff1 = BRANCH_LENGTH_CUTOFF1
			#cut by the first cutoff
			print i,"Cutting branches longer than",cutoff1
			outfile = open(inDIR+clusterID+".cut1.trees","w")
			newtrees = [] #store trees in need of cutting for the next round
			count = 0
			while True:
				for tree in trees:
					if find_longest_branch_length(tree) < cutoff1:
						outfile.write(newick3.tostring(tree) +";\n")
					else:
						subtrees = cut_long_branches(tree,cutoff1)
						for subtree in subtrees:
							if count_taxa(subtree) >= MIN_TAXA:
								count += 1
								newname = inDIR+clusterID+".cut1-"+str(count)
								#record the branch cut
								with open(newname+".cutbranch","w") as outfile1:
									outfile1.write(newick3.tostring(subtree)+";\n")
								#output fasta from the branch cut
								with open(newname+".fa","w") as outfile2:
									for label in get_leaf_labels(subtree.leaves()):
										outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
								newaln = mafft_align(newname+".fa")
								#trim with occupancy 0.1 since the alignment may be sparse					
								phyutility(newaln,newaln+"-cln",0.1)
								#trimal(newaln,newaln+"-cln",0.3)
								newtreefile = fasttree(newaln+"-cln")
								with open(newtreefile) as infile: #update the tree
									newtrees.append(newick3.parse(infile.readline()))
				if newtrees == []: break
				trees = newtrees
				newtrees = []
			outfile.close()
			trees = [] #update the tree record for the next round
			with open(inDIR+clusterID+".cut1.trees","r") as infile:
				for line in infile:
					if len(line) > 3: trees.append(newick3.parse(line))
		
		"""Step2: cut with the second branch length cutoff"""
		if len(trees) == 0 or (trees[0] == None):
			with open(out_error,"a") as outfile:
				outfile.write("check "+fasta_file+"\n")
			continue
		print len(trees),"trees cutting by",BRANCH_LENGTH_CUTOFF2
		outfile = open(inDIR+clusterID+".cut2-final.trees","w")
		count = 0
		newtrees = [] #store trees in need of cutting for the next round
		while True:
			for tree in trees:
				if find_longest_internal_branch_length(tree) < BRANCH_LENGTH_CUTOFF2:
					#when only tips are too long, cut them off without re-align
					subtrees = cut_long_branches(tree,BRANCH_LENGTH_CUTOFF2)
					for subtree in subtrees:
						if count_taxa(subtree) >= MIN_TAXA:
							outfile.write(newick3.tostring(subtree)+";\n")
				else: #has internal long branches
					subtrees = cut_long_branches(tree,BRANCH_LENGTH_CUTOFF2)
					for subtree in subtrees:
						if count_taxa(subtree) >= MIN_TAXA:
							count += 1
							newname = inDIR+clusterID+".cut2-"+str(count)
							#record the branch cut
							with open(newname+".cutbranch","w") as outfile1:
								outfile1.write(newick3.tostring(subtree)+";\n")
							#output fasta from the branch cut
							with open(newname+".fa","w") as outfile2:
								for label in get_leaf_labels(subtree.leaves()):
									outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
							newaln = mafft_align(newname+".fa")
							#trim with occupancy 0.3, more stringent
							trimal(newaln,newaln+"-cln",0.3)
							newtreefile = fasttree(newaln+"-cln")
							with open(newtreefile) as infile: #update the tree
								newtrees.append(newick3.parse(infile.readline()))
			if newtrees == []: break
			trees = newtrees
			newtrees = []
		outfile.close()

		infile = open(inDIR+clusterID+".cut2-final.trees","r")
		count = 0
		for line in infile:
			if len(line) < 3: continue
			count += 1
			tree = newick3.parse(line)
			newfasta = outDIR+clusterID+"-"+str(count)+".final.fa"
			with open(newfasta,"w") as outfile2: #output fasta
				for label in get_leaf_labels(tree.leaves()):
					outfile2.write(">"+label+"\n"+seqDICT[label]+"\n")
		if count == 0:
			with open(out_error,"a") as outfile:
				outfile.write("check "+fasta_file+"\n")
		infile.close()

