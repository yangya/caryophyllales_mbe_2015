"""
Read the fasta file used for all-by-all blast and the connected components
write individual fasta files and edge files for each connected components
discard sequences that are very short
"""

import sys,os
from Bio import SeqIO

if __name__ =="__main__":
	if len(sys.argv) != 4:
		print "usage: write_fasta_files_from_ccs.py .fa .ccs outDIR"
		sys.exit()
	
	print "Reading clusters"
	ccDICT = {} #cc stands for connected component
	#key is taxon ID, value is seqID
	infile = open(sys.argv[2],"rU")
	for line in infile:
		if len(line) < 3: continue #ignore empty lines
		line = line.strip().split(":")
		ccID = line[0]
		spls = line[1].split('\t')
		for seqID in spls:
			taxonID = seqID.split("@")[1]
			if taxonID not in ccDICT:
				ccDICT[taxonID] = {}
			ccDICT[taxonID][seqID] = ccID
	infile.close()
	
	print "Reading the fasta file"
	handle = open(sys.argv[1],"rU")
	outDICT = {} #key is ccID, value is a s of fasta output
	for seq_record in SeqIO.parse(handle,"fasta"):
		seqid,seq = str(seq_record.id),str(seq_record.seq)
		taxonID = seqid.split("@")[1]
		#seq that only hit itself in blastn will be missing from the ccDICT
		try: ccID = ccDICT[taxonID][seqid]
		except: continue
		if ccID not in outDICT:
			outDICT[ccID] = ""
		outDICT[ccID] += ">"+seqid+"\n"+seq+"\n"
	handle.close()

	print "Writing fasta files"
	for ccID in outDICT:
		with open(sys.argv[3]+"/"+str(ccID)+".fa","w") as outfile:
			outfile.write(outDICT[ccID])
